import React, { Component } from "react";

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = { email: "", password: "", formSubmitted: false };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEmailChange(event) {
    this.setState({ email: event.target.value });
  }
  handlePasswordChange(event) {
    this.setState({ password: event.target.value });
  }
  handleSubmit(event) {
    alert("A name was submitted: " + this.state.email + this.state.password);
    this.setState({ formSubmitted: true });
    event.preventDefault();
  }

  render() {
    return (
      <>
        {!this.state.formSubmitted && (
          <form onSubmit={this.handleSubmit}>
            <label>
              Email:
              <input type="email" value={this.state.email} onChange={this.handlePasswordChange} />
            </label>
            <label>
              password:
              <input type="password" value={this.state.password} onChange={this.handlePasswordChange} />
            </label>
            <div>
              <button type="submit" className="btn btn-primary">
                submit
              </button>{" "}
            </div>
          </form>
        )}
        {
          <di>
            <h1>Email: {this.state.email}</h1>
            <h1>Password: {this.setState.password}</h1>
          </di>
        }
      </>
    );
  }
}

export default Register;
